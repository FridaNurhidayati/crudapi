﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LatihanNetClass.Controllers
{
    [RoutePrefix("api/Category")]
    public class CategoryController : ApiController
    {
        [Route("readAll")]
        [HttpGet]
        public IHttpActionResult ReadAll()
        {
            var db = new DB_Context();
            try
            {
                var listCategoryEntity = db.Categories.ToList();
                List<CategoryViewModel> listProduct = new List<CategoryViewModel>();
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (var item in listCategoryEntity)
                {
                    CategoryViewModel product = new CategoryViewModel()
                    {
                        CategoryID = item.CategoryID,
                        CategoryName = item.CategoryName,
                        Descripttion = item.Description,
                        Picture = item.Picture
                    };

                    listProduct.Add(product);
                };

                result.Add("Message", "Read data success");
                result.Add("Data", listProduct);

                db.Dispose();
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }

        }


        [Route("create")]
        [HttpPost]

        public IHttpActionResult Create([FromBody] CategoryViewModel dataBody)
        {
            var Db = new DB_Context();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Category newCategory = new Category()
                {
                    CategoryID = dataBody.CategoryID,
                    Description = dataBody.Descripttion,
                    CategoryName = dataBody.CategoryName,
                    Picture = dataBody.Picture
                };

                Db.Categories.Add(newCategory);
                Db.SaveChanges();
                Db.Dispose();
                result.Add("Message", "Insert Data Success");
                return Ok(result);

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("update")]
        [HttpPut]

        public IHttpActionResult Update([FromBody] CategoryViewModel dataBody)
        {
            var Db = new DB_Context();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Category category = Db.Categories.Find(dataBody.CategoryID);
                category.CategoryID = dataBody.CategoryID;
                category.Description = dataBody.Descripttion;
                category.Picture = dataBody.Picture;

                Db.SaveChanges();

                Db.Dispose();
                result.Add("Message", "Update Data Success");
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int categoryId)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Category category = db.Categories.Where(data => data.CategoryID == categoryId).FirstOrDefault();
                    db.Categories.Remove(category);
                    db.SaveChanges();
                    result.Add("Message", "Delete data success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
       
    }
   
}