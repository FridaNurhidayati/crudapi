﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LatihanNetClass.Controllers
{
    [RoutePrefix("api/OrderDetail")]
    public class OrderDetailController:ApiController
    {

        [Route("custom")]
        [HttpGet]

        public IHttpActionResult Custom(int? OrderId = null)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    List<OrderViewModel> listOrderDetailView = new List<OrderViewModel>(); //buat list untuk menampung order detail nya
                    var data_order = db.Orders.AsQueryable();   // data order yang dicari (msh dalam bentuk queryable (msh bisa digmna gmnain utk nyari data) 
                    if (OrderId != null)        //Tentukan kondisi jika order id nya tidak diisi atw diisi
                    {
                        data_order = data_order.Where(item => item.OrderID == OrderId);
                    }
                    var list_order = data_order.AsEnumerable().ToList();    //ubah ke dalam list 
                    foreach (var order in list_order)
                    {
                        OrderViewModel detail_order = new OrderViewModel(order); //mapping satu-satu ke dalam 
                        listOrderDetailView.Add(detail_order);      //Add ke dalam list order detail view
                    }
                    result.Add("Message", "Read data Success");
                    result.Add("Data", listOrderDetailView);
                    return Ok(result);
                }
                catch(Exception)
                {
                    throw;
                }
            }
        }

        [Route("customer_filter")]
        [HttpGet]

        public IHttpActionResult CustomFilter(string CustomerId = null)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    List<CustomerView> listCustomerOrderDetailView = new List<CustomerView>(); 
                    var data_customer = db.Customers.AsQueryable();    
                    if (CustomerId != null)        
                    {
                        data_customer = data_customer.Where(item => item.CustomerID == CustomerId);
                    }
                    var list_customer_order = data_customer.AsEnumerable().ToList();    
                    foreach (var order in list_customer_order)
                    {
                        CustomerView detail_customer_order = new CustomerView()
                        {
                            CustomerID = order.CustomerID,
                            CustomerName = order.ContactName,
                            product_list = order.Orders.ToList().Select(data => new OrderViewModel(data)).ToList()
                        };
                        listCustomerOrderDetailView.Add(detail_customer_order);     
                    }
                    result.Add("Message", "Read data Success");
                    result.Add("Data", listCustomerOrderDetailView);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }
}