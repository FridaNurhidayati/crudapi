﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LatihanNetClass.Controllers
{
    [RoutePrefix("api/Product")]
    public class ProductController : ApiController
    {
        [Route("readAll")]
        [HttpGet]

        public IHttpActionResult ReadAll()
        {
            var db = new DB_Context();
            try
            {
                var listProductEntity = db.Products.ToList();
                List<ProductViewModel> listProduct = new List<ProductViewModel>();
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (var item in listProductEntity)
                {
                    ProductViewModel product = new ProductViewModel()
                    {
                        ProductID = item.ProductID,
                        ProductName = item.ProductName,
                        SupplierID = item.SupplierID,
                        CategoryID = item.CategoryID,
                        QuantityPerUnit = item.QuantityPerUnit,
                        UnitPrice = item.UnitPrice,
                        UnitsInStock = item.UnitsInStock,
                        UnitsOnOrder = item.UnitsOnOrder,
                        ReorderLevel = item.ReorderLevel,
                        Discontinued = item.Discontinued

                    };

                    listProduct.Add(product);
                };

                result.Add("Message", "ReadAll data Success");
                result.Add("Data", listProduct);

                db.Dispose();
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("create")]
        [HttpPost]

        public IHttpActionResult Create([FromBody] ProductViewModel dataBody)
        {
            var Db = new DB_Context();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Product newProduct = new Product()
                {
                    ProductID = dataBody.ProductID,
                    ProductName = dataBody.ProductName,
                    SupplierID = dataBody.SupplierID,
                    CategoryID = dataBody.CategoryID,
                    QuantityPerUnit = dataBody.QuantityPerUnit,
                    UnitPrice = dataBody.UnitPrice,
                    UnitsInStock = dataBody.UnitsInStock,
                    UnitsOnOrder = dataBody.UnitsOnOrder,
                    ReorderLevel = dataBody.ReorderLevel,
                    Discontinued = dataBody.Discontinued
                
                };

                Db.Products.Add(newProduct);
                Db.SaveChanges();
                Db.Dispose();
                result.Add("Message", "Insert Data Success");
                return Ok(result);

            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("update")]
        [HttpPut]

        public IHttpActionResult Update([FromBody] ProductViewModel dataBody)
        {
            var Db = new DB_Context();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Product product = Db.Products.Find(dataBody.ProductID);
                product.ProductID = dataBody.ProductID;
                product.ProductName = dataBody.ProductName;
                product.SupplierID = dataBody.SupplierID;
                product.CategoryID = dataBody.CategoryID;
                product.QuantityPerUnit = dataBody.QuantityPerUnit;
                product.UnitPrice = dataBody.UnitPrice;
                product.UnitsInStock = dataBody.UnitsInStock;
                product.UnitsOnOrder = dataBody.UnitsOnOrder;
                product.ReorderLevel = dataBody.ReorderLevel;
                product.Discontinued = dataBody.Discontinued;
                Db.SaveChanges();

                Db.Dispose();
                result.Add("Message", "Update Data Success");
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int productId)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Product product = db.Products.Where(data => data.ProductID == productId).FirstOrDefault();
                    db.Products.Remove(product);
                    db.SaveChanges();
                    result.Add("Message", "Delete data success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [Route("custom")]
        [HttpGet]

        public IHttpActionResult Custom()
        {
           using (var db = new DB_Context()) 
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                var max_price = db.Products.Max(data => data.UnitPrice);
                var expensive_product = db.Products.Where(data => data.UnitPrice == max_price).ToList().Select(item => new ProductViewModel(item));
                IEnumerable<ProductViewModel> cheap_product = db.Products.Where(data => data.UnitPrice == db.Products.Min(item => item.UnitPrice))
                    .ToList().Select(x => new ProductViewModel(x));
                var average_price = db.Products.Average(data => data.UnitPrice);
                IEnumerable<ProductViewModel> under_average_price = db.Products.Where(data => data.UnitPrice < db.Products.Average(item => item.UnitPrice))
                    .ToList().Select(x => new ProductViewModel(x));
                var under_total = under_average_price.Count();
                result.Add("ExpensiveProduct", expensive_product);
                result.Add("CheapProduct", cheap_product);
                result.Add("AveragePrice", average_price);
                result.Add("UnderAverage", under_average_price);
                result.Add("UnderAverageTotals", under_total);
                return Ok(result);              
            }
            catch (Exception)
            {
                throw;
            }

        }

        [Route("filter")]
        [HttpGet]

        public IHttpActionResult Filter(string productName = null, string categoryName = null, decimal? price = null)
        {
            using (var db = new DB_Context())
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var temp = db.Products.AsQueryable();
                    if(productName != null)
                    {
                        temp = temp.Where(data => data.ProductName.ToLower().Contains(productName.ToLower()));
                    }
                    if(categoryName != null)
                    {
                        temp = temp.Where(data => data.Category.CategoryName.ToLower().Contains(categoryName.ToLower()));
                    }
                    if(price != null)
                    {
                        temp = temp.Where(data => data.UnitPrice < price);
                    }

                    var listProduct = temp.ToList().Select(data => new ProductViewModel(data));

                    result.Add("Message", "Read data success");
                    result.Add("Data", listProduct);
                    return Ok(result);
                }
                catch(Exception)
                {
                    throw;
                }
        }
    }
}