﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels;
using LatihanNetClass.ViewModels.Calculation;
using LatihanNetClass.ViewModels.ProductDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LatihanNetClass.Controllers
{
    [RoutePrefix("api/ProductDetail")]
    public class ProductDetailController : ApiController
    {
        //[Route("create")]
        //[HttpPost]

        //public IHttpActionResult Create([FromBody] ProductDetailViewModel dataBody, string condition = "", int userDemand = 0, int duration = 0)
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {
        //            //Dictionary<string, object> result = new Dictionary<string, object>();
        //            ProductDetailViewModel product_detail = new ProductDetailViewModel();
        //            Product product = new Product();
        //            product = dataBody.ConvertToProduct(condition, userDemand, duration);
        //            db.Products.Add(product);
        //            db.SaveChanges();
        //            return Ok("Insert Data Berhasil");
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        //}


        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int productId)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Product product = db.Products.Where(data => data.ProductID == productId).FirstOrDefault();
                    db.Products.Remove(product);
                    db.SaveChanges();
                    result.Add("Message", "Delete data success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        //[Route("read")]
        //[HttpGet]

        //public IHttpActionResult Read(int? productID = null, char? delimiter = null)
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {
        //            Dictionary<string, object> result = new Dictionary<string, object>();
        //            List<ProductDetailViewModel> product_detail = new List<ProductDetailViewModel>();
        //            var data_product = db.Products.AsQueryable();
        //            if (productID != null)
        //            {
        //                data_product = data_product.Where(data => data.ProductID == productID);
        //            }
        //            //var listProduct = data_product.AsEnumerable().ToList();
        //            foreach (var data in data_product)
        //            {
        //                ProductDetailViewModel detail_product = new ProductDetailViewModel(data, delimiter); //mapping satu-satu ke dalam 
        //                product_detail.Add(detail_product);      //Add 
        //            }
        //            result.Add("Message", "Read data Success");
        //            result.Add("Items", product_detail);
        //            return Ok(result);
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        //}

        //[Route("getTransportationCostCalculation")]
        //[HttpGet]
        //public IHttpActionResult GetTransportationCostCalculation(string Condition, int UserDemand)
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {

        //            Dictionary<string, object> result = new Dictionary<string, object>();
        //            List<TransportationCostCalculation> product_detail = new List<TransportationCostCalculation>();
        //            var data_product = db.Products.AsQueryable();
        //            data_product = data_product.Where(data => data.ProductType.Contains("TransportationServices"));
        //            var listProduct = data_product.AsEnumerable().ToList();
        //            foreach (var data in listProduct)
        //            {
        //                TransportationCostCalculation detail_product = new TransportationCostCalculation(data, Condition, UserDemand); //mapping satu-satu ke dalam 
        //                product_detail.Add(detail_product);      //Add 
        //            }
        //            result.Add("Message", "Read data Success");
        //            result.Add("Items", product_detail);
        //            return Ok(result);



        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        //}

        [Route("calculateProductUnitPrice")]
        [HttpPost]
        public IHttpActionResult calculateProductUnitPrice([FromBody] ProductDetailCalculatorParameter parameter)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    var temp = db.Products.AsQueryable();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var listProduct = db.Products.OrderByDescending(data => data.ProductID).ToList();

                    ProductCalculator calculator = new ProductCalculator(';');
                    foreach (var item in listProduct)
                    {
                        calculator.calculateProductUnitPrice(item, parameter);
                    }

                    db.SaveChanges();
                    return Ok("Data Saved Successfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("countInvalidProductDetail")]
        [HttpGet]
        public IHttpActionResult countInvalidProductDetail(ProductDetail productDetail, string tipe)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    ProductValidator valid = new ProductValidator();
                    //ProductDetail detail = new ProductDetail();
                    var count = 0;
                    var data_product = db.Products.AsQueryable();
                    var listProduct = data_product.AsEnumerable().ToList();
                    foreach (var data in listProduct)
                    {
                        if(valid.IsValidProductDetail(productDetail,tipe) == false)
                        {
                            count = count + 1;
                        }
                    }
                    result.Add("Message", "Data Invalid");
                    result.Add("Totals", count);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [Route("createProductWithStringProductDetail")]
        [HttpPost]
        public IHttpActionResult createProductWithStringProductDetail([FromBody] ProductDetailCreate dataBody, string productDetail, string productType)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    ProductDetailCreate product_detail = new ProductDetailCreate();
                    Product product = new Product();
                    ProductValidator valid = new ProductValidator();
                    if (valid.IsValidProductDetail(productDetail, productType) == true)
                    {
                        product = dataBody.ConvertToProduct();
                        db.Products.Add(product);
                        db.SaveChanges();
                        return Ok("Insert Data Berhasil");
                    }
                    else
                    {
                        result.Add("Message", "Data Insert Invalid");
                        return Ok(result);
                    }
                                      
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("createProductWithProductDetail")]
        [HttpPost]
        public IHttpActionResult createProductWithProductDetail([FromBody] ProductDetailCreate dataBody, Dictionary<string, object> productDetail, string productType)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    ProductDetailCreate product_detail = new ProductDetailCreate();
                    Product product = new Product();
                    ProductValidator valid = new ProductValidator();
                    if (valid.IsValidProductDetail(productDetail, productType) == true)
                    {
                        product = dataBody.ConvertToProduct();
                        db.Products.Add(product);
                        db.SaveChanges();
                        return Ok("Insert Data Berhasil");
                    }
                    else
                    {
                        result.Add("Message", "Data Insert Invalid");
                        return Ok(result);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}