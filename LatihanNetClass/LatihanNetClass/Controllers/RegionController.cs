﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LatihanNetClass.Controllers
{
    [RoutePrefix("api/Region")]
    public class RegionController :ApiController
    {
        [Route("read")]
        [HttpGet]

        public IHttpActionResult Read(int? regionID = null)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    List<RegionDetail> region_detail = new List<RegionDetail>();
                    var data_region = db.Regions.AsQueryable();
                    if (regionID != null)
                    {
                        data_region = data_region.Where(data => data.RegionID == regionID);
                    }
                    var listRegion = data_region.AsEnumerable().ToList();
                    foreach (var region in data_region)
                    {
                        RegionDetail detail_region = new RegionDetail(region); //mapping satu-satu ke dalam 
                        region_detail.Add(detail_region);      //Add 
                    }
                    result.Add("Message", "Read data Success");
                    result.Add("Data", region_detail);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [Route("create")]
        [HttpPost]

        public IHttpActionResult Create([FromBody] RegionDetail dataBody)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    List<RegionDetail> region_detail = new List<RegionDetail>();
                    var newRegion = dataBody.convertToRegion();
                    db.Regions.Add(newRegion);
                    db.SaveChanges();
                    dataBody.toTerritority(db);
                    var data_region = db.Regions.Where(data => data.RegionID == dataBody.RegionID).AsEnumerable().ToList();
                    foreach (var region in data_region)
                    {
                        RegionDetail detail_region = new RegionDetail(region); //mapping satu-satu ke dalam 
                        region_detail.Add(detail_region);      //Add 
                    }
                    result.Add("Data", region_detail);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [Route("update")]
        [HttpPut]

        public IHttpActionResult Update([FromBody] RegionDetail dataBody)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    List<RegionDetail> region_detail = new List<RegionDetail>();
                    Region region = db.Regions.Find(dataBody.RegionID);
                    var newRegion = dataBody.convertToRegion();
                    db.Regions.Add(newRegion);
                    db.SaveChanges();
                    var data_region = db.Regions.Where(data => data.RegionID == dataBody.RegionID).AsEnumerable().ToList();
                    foreach (var data in data_region)
                    {
                        RegionDetail detail_region = new RegionDetail(data); //mapping satu-satu ke dalam 
                        region_detail.Add(detail_region);      //Add 
                    }
                    result.Add("Message", "Update Data Success");
                    result.Add("Data", region_detail);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int regionId)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Region region = db.Regions.Where(data => data.RegionID == regionId).FirstOrDefault();
                    db.Regions.Remove(region);
                    db.SaveChanges();
                    result.Add("Message", "Delete data success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}