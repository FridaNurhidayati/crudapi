﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels.ProductDetails;
using LatihanNetClass.ViewModels.ProductDetails.Items.Details;
using LatihanNetClass.ViewModels.ProductDetails.Services.Details;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.Calculation
{
    public class ProductCalculator
    {
        private char Delimiter;
        public ProductCalculator(char Delimiter)
        {
            this.Delimiter = Delimiter;
        }
        public void calculateProductUnitPrice(Product product, ProductDetailCalculatorParameter parameter)
        {
            ProductDetail productDetail = null;
            if (product.ProductType != null)
            {
                if (product.ProductType.Contains("FoodsAndBeverageItems"))
                {
                    productDetail = new FoodAndBeverage(this.Delimiter, product);
                }else if (product.ProductType.Contains("MaterialItems"))
                {
                    productDetail = new Material(this.Delimiter, product);
                }else if (product.ProductType.Contains("GarmentItems"))
                {
                    productDetail = new Garment(this.Delimiter, product);
                }else if (product.ProductType.Contains("TransportationServices"))
                {
                    productDetail = new Transportation(this.Delimiter, product);
                }else if (product.ProductType.Contains("TelecommunicationServices"))
                {
                    productDetail = new Telecommunication(this.Delimiter, product);
                }
                else
                {
                    throw new Exception("Unknown Product Type");
                }
                productDetail.setAdditionalParameter(parameter);
                product.UnitPrice = productDetail.calculateProductCost() * productDetail.getDecCostRate();

            }
        }
    }
}