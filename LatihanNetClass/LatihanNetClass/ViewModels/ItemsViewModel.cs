﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    interface ItemsViewModel 
    {

        char delimiter();
        Dictionary<string, object> Prod();
        String ConvertItemsToString();
        decimal CalculateProducTotUnitPrice();

    }

   
}