﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class OrderViewModel
    {
        public int? OrderID { get; set; }
        //public string ContactName { get; set; }
        public List<OrderDetailViewModel> product_list { get; set; }
        public decimal GrandTotal { get; set; }
        public int ProductTotal { get; set; }
        public OrderViewModel (Order entity)
        {
            OrderID = entity.OrderID;
            //ContactName = entity.Customer.ContactName;
            product_list = entity.Order_Details.ToList().Select(data => new OrderDetailViewModel(data)).ToList();
            GrandTotal = entity.Order_Details.ToList().Select(data => new OrderDetailViewModel(data)).ToList().Sum(data => data.Total);
            ProductTotal = entity.Order_Details.ToList().Select(data => new OrderDetailViewModel(data)).ToList().Count();
        } 
    }
}