﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class ProductDetailFoodsAndBeverageItems : ItemsViewModel
    {
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string ProductionCode { get; set; }
        public string ProductionDate { get; set; }
        public string ExpiredDate { get; set; }
        public string NetWeight { get; set; }
        public string Ingredients { get; set; }
        public string DailyValue { get; set; }
        public string Certification { get; set; }
        public string CostRate { get; set; }
        public string UnitOfMeasurement { get; set; }
        public char delimiter()
        {
            return '\'';
        }

        public ProductDetailFoodsAndBeverageItems()
        {

        }
        public Dictionary<string, object> Prod()
        {
            Dictionary<string, object> foodDict = new Dictionary<string, object>();
            foodDict.Add("ProductID", this.ProductID);
            foodDict.Add("ProductDescription", this.ProductDescription);
            foodDict.Add("ProductionCode", this.ProductionCode);
            foodDict.Add("ProductionDate", this.ProductionDate);
            foodDict.Add("ExpiredDate", this.ExpiredDate);
            foodDict.Add("NetWeight", this.NetWeight);
            foodDict.Add("Ingredients", this.Ingredients);
            foodDict.Add("DailyValue", this.DailyValue);
            foodDict.Add("Certification", this.Certification);
            foodDict.Add("CostRate", this.CostRate);
            foodDict.Add("UnitOfMeasurement", this.UnitOfMeasurement);
            return foodDict;
        }
        public ProductDetailFoodsAndBeverageItems(Product product)
        {
            this.ProductID = product.ProductID.ToString();
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] pisah = product.ProductDetail.Split(delimiter());
                this.ProductDescription = pisah[0];
                this.ProductionCode = pisah[1];
                this.ProductionDate = pisah[2];
                this.ExpiredDate = pisah[3];
                this.NetWeight = pisah[4];
                this.Ingredients = pisah[5];
                this.DailyValue = pisah[6];
                this.Certification = pisah[7];
                this.CostRate = pisah[8];
                this.UnitOfMeasurement = pisah[9];
            }
        }

        public string ConvertItemsToString()
        {
            return this.ProductDescription + delimiter() +
                this.ProductionCode + delimiter() +
                this.ProductionDate + delimiter() +
                this.ExpiredDate + delimiter() +
                this.NetWeight + delimiter() +
                this.Ingredients + delimiter() +
                this.DailyValue + delimiter() +
                this.Certification + delimiter() +
                this.CostRate + delimiter() +
                this.UnitOfMeasurement ;
        }

        public decimal CalculateProducTotUnitPrice()
        {
            var result = Decimal.Parse(CostRate) * (Convert.ToDecimal(110) / Convert.ToDecimal(100));
            return result;
        }
    }
}