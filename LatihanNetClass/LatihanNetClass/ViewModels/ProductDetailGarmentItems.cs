﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class ProductDetailGarmentItems : ItemsViewModel
    {
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string ProductionCode { get; set; }
        public string ProductionDate { get; set; }
        public string GarmentsType { get; set; }
        public string Fabrics { get; set; }
        public string GenderRelated { get; set; }
        public string IsWaterProof { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string AgeGroup { get; set; }
        public string CostRate { get; set; }
        public string UnitOfMeasurement { get; set; }
        public char delimiter()
        {
            return '\'';
        }
        public ProductDetailGarmentItems()
        {

        }
        public Dictionary<string, object> Prod()
        {
            Dictionary<string, object> garmentDict = new Dictionary<string, object>();
            garmentDict.Add("ProductID", this.ProductID);
            garmentDict.Add("ProductDescription", this.ProductDescription);
            garmentDict.Add("ProductionCode", this.ProductionCode);
            garmentDict.Add("ProductionDate", this.ProductionDate);
            garmentDict.Add("GarmentsType", this.GarmentsType);
            garmentDict.Add("Fabrics", this.Fabrics);
            garmentDict.Add("GenderRelated", this.GenderRelated);
            garmentDict.Add("IsWaterProof", this.IsWaterProof);
            garmentDict.Add("Color", this.Color);
            garmentDict.Add("Size", this.Size);
            garmentDict.Add("AgeGroup", this.AgeGroup);
            garmentDict.Add("CostRate", this.CostRate);
            garmentDict.Add("UnitOfMeasurement", this.UnitOfMeasurement);
            return garmentDict;
        }

        public ProductDetailGarmentItems(Product product)
        {
            this.ProductID = product.ProductID.ToString();

            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] pisah = product.ProductDetail.Split(delimiter());
                this.ProductDescription = pisah[0];
                this.ProductionCode = pisah[1];
                this.ProductionDate = pisah[2];
                this.GarmentsType = pisah[3];
                this.Fabrics = pisah[4];
                this.GenderRelated = pisah[5];
                this.IsWaterProof = pisah[6];
                this.Color = pisah[7];
                this.Size = pisah[8];
                this.AgeGroup = pisah[9];
                this.CostRate = pisah[10];
                this.UnitOfMeasurement = pisah[11];

            }
        }

        public string ConvertItemsToString()
        {
            return this.ProductDescription + delimiter() +
                this.ProductionCode + delimiter() +
                this.ProductionDate + delimiter() +
                this.GarmentsType + delimiter() +
                this.Fabrics + delimiter() +
                this.GenderRelated + delimiter() +
                this.IsWaterProof + delimiter() +
                this.Color + delimiter() +
                this.Size + delimiter() +
                this.AgeGroup + delimiter() +
                this.CostRate + delimiter() +
                this.UnitOfMeasurement ;
        }

        public decimal CalculateProducTotUnitPrice()
        {
            var result = Decimal.Parse(CostRate) * (Convert.ToDecimal(110) / Convert.ToDecimal(100));
            return result;
        }
    }
}