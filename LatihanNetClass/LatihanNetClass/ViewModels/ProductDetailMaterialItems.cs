﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class ProductDetailMaterialItems : ItemsViewModel
    {
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string ProductionCode { get; set; }
        public string ProductionDate { get; set; }
        public string ExpiredDate { get; set; }
        public string MaterialsType { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string IsConsumable { get; set; }
        public string CostRate { get; set; }
        public char delimiter()
        {
            return '\'';
        }
        public ProductDetailMaterialItems()
        {

        }

        public Dictionary<string, object> Prod()
        {
            Dictionary<string, object> materiDict = new Dictionary<string, object>();
            materiDict.Add("ProductID", this.ProductID);
            materiDict.Add("ProductDescription", this.ProductDescription);
            materiDict.Add("ProductionCode", this.ProductionCode);
            materiDict.Add("ProductionDate", this.ProductionDate);
            materiDict.Add("ExpiredDate", this.ExpiredDate);
            materiDict.Add("MaterialsType", this.MaterialsType);
            materiDict.Add("UnitOfMeasurement", this.UnitOfMeasurement);
            materiDict.Add("IsConsumable", this.IsConsumable);
            materiDict.Add("CostRate", this.CostRate);
            return materiDict;
        }
        public ProductDetailMaterialItems(Product product)
        {
            this.ProductID = product.ProductID.ToString(); ;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] pisah = product.ProductDetail.Split(delimiter());
                this.ProductDescription = pisah[0];
                this.ProductionCode = pisah[1];
                this.ProductionDate = pisah[2];
                this.ExpiredDate = pisah[3];
                this.MaterialsType = pisah[4];
                this.UnitOfMeasurement = pisah[5];
                this.IsConsumable = pisah[6];
                this.CostRate = pisah[7];

            }
        }

        public string ConvertItemsToString()
        {
            return this.ProductDescription + delimiter() +
                this.ProductionCode + delimiter() +
                this.ProductionDate + delimiter() +
                this.ExpiredDate + delimiter() +
                this.MaterialsType + delimiter() +
                this.UnitOfMeasurement + delimiter() +
                this.IsConsumable + delimiter() +
                this.CostRate;
        }
        public decimal CalculateProducTotUnitPrice()
        {
            var result = Decimal.Parse(CostRate) * (Convert.ToDecimal(110) / Convert.ToDecimal(100));
            return result;
        }
    }
}