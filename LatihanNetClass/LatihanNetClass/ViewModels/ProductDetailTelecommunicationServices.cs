﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class ProductDetailTelecommunicationServices : ServicesViewModel
    {
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string PacketType { get; set; }
        public string PacketLimit { get; set; }
        public string CostCalculationMethod { get; set; }
        public string CostRate { get; set; }
        public char delimiter()
        {
            return '\'';
        }

        public ProductDetailTelecommunicationServices()
        {

        }
        public Dictionary<string, object> Prod()
        {
            Dictionary<string, object> telecommunicationiDict = new Dictionary<string, object>();
            telecommunicationiDict.Add("ProductID", this.ProductID);
            telecommunicationiDict.Add("ProductDescription", this.ProductDescription);
            telecommunicationiDict.Add("PacketType", this.PacketType);
            telecommunicationiDict.Add("PacketLimit", this.PacketLimit);
            telecommunicationiDict.Add("CostCalculationMethod", this.CostCalculationMethod);
            telecommunicationiDict.Add("CostRate", this.CostRate);
            return telecommunicationiDict;
        }

        public ProductDetailTelecommunicationServices(Product product)
        {
            //char[] delimiter = { ';' };
            this.ProductID = product.ProductID.ToString();
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] pisah = product.ProductDetail.Split(delimiter());
                this.ProductDescription = pisah[0];
                this.PacketType = pisah[1];
                this.PacketLimit = pisah[2];
                this.CostCalculationMethod = pisah[3];
                this.CostRate = pisah[4];

            }
        }

        public string ConvertServicesToString()
        {
            return this.ProductDescription + delimiter() +
                this.PacketType + delimiter() +
                this.PacketLimit + delimiter() +
                this.CostCalculationMethod + delimiter() +
                this.CostRate + delimiter();
        }

        public decimal CalculateProducTotUnitPrice(string condition = "", int userDemand = 0, int duration = 0)
        {
            decimal result = 0;
            if (this.CostCalculationMethod.Equals("PerSecond"))
            {
                result = Convert.ToDecimal(this.CostRate) * duration;
            }
            else if (this.CostCalculationMethod.Equals("PerPacket"))
            {
                if (this.PacketType.Equals("Data"))
                {
                    result = Convert.ToDecimal(this.PacketLimit) * Convert.ToDecimal(this.CostRate);
                }
                else
                {
                    result = (Convert.ToDecimal(this.CostRate) * duration) * Convert.ToDecimal(this.CostRate);
                }
            }
            result = result * (Convert.ToDecimal(110) / Convert.ToDecimal(100));
            return result;
        }

    }
}