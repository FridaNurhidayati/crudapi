﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class ProductDetailTransportaionServices : ServicesViewModel
    {
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string VehicleType { get; set; }
        public string RoutePath { get; set; }
        public string RouteMileage { get; set; }
        public string CostCalculationMethod { get; set; }
        public string CostRate { get; set; }
        //public int CostCalculation { get; set; }
        public char delimiter()
        {
            return '\'';
        }
        public ProductDetailTransportaionServices()
        {

        }
        public Dictionary<string, object> Prod()
        {
            Dictionary<string, object> transportasiDict = new Dictionary<string, object>();
            transportasiDict.Add("ProductID", this.ProductID);
            transportasiDict.Add("ProductDescription", this.ProductDescription);
            transportasiDict.Add("VehicleType", this.VehicleType);
            transportasiDict.Add("RoutePath", this.RoutePath);
            transportasiDict.Add("RouteMileage", this.RouteMileage);
            transportasiDict.Add("CostCalculationMethod", this.CostCalculationMethod);
            transportasiDict.Add("CostRate", this.CostRate);
            return transportasiDict;
        }

        public ProductDetailTransportaionServices(Product product)
        {
            this.ProductID = product.ProductID.ToString();
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] pisah = product.ProductDetail.Split(delimiter());
                this.ProductDescription = pisah[0];
                this.VehicleType = pisah[1];
                this.RoutePath = pisah[2];
                this.RouteMileage = pisah[3];
                this.CostCalculationMethod = pisah[4];
                this.CostRate = pisah[5];
                //this.CostCalculation = Kalkulasi(Condition, UserDemand);
            }
        }

        public string ConvertServicesToString()
        {
            return this.ProductDescription + delimiter() +
                this.VehicleType + delimiter() +
                this.RoutePath + delimiter() +
                this.RouteMileage + delimiter() +
                this.CostCalculationMethod + delimiter() +
                this.CostRate;
        }


        //public decimal CalculateServiceProductUnitPrice()
        //{
        //    //var costCalculation = Kalkulasi(Condition,UserDemand);
        //    var result = CostCalculation * (Convert.ToDecimal(110) / Convert.ToDecimal(100));
        //    return result;
        //}

        public decimal CalculateProducTotUnitPrice(string Condition = "", int UserDemand = 0, int duration = 0)
        {
            var result = 0;
            if (this.CostCalculationMethod.Equals("FixPerRoute"))
            {
                result = 1 * Convert.ToInt32(this.CostRate);
            }
            else if (this.CostCalculationMethod.Equals("PerMiles"))
            {
                result = (Convert.ToInt32(this.RouteMileage) * Convert.ToInt32(this.CostRate)) / 2;
            }
            else if (this.CostCalculationMethod.Equals("PerMilesWithCondition"))
            {
                var valcon = 0;
                if (Condition == "GoodWeather")
                {
                    valcon = 5;
                }
                else if (Condition == "BadWeather")
                {
                    valcon = 15;
                }
                result = ((Convert.ToInt32(this.RouteMileage) * Convert.ToInt32(this.CostRate)) / 2) * (((valcon + (UserDemand / 50)) + 95) / 100);
            }
            return result * (Convert.ToDecimal(110) / Convert.ToDecimal(100));
        }

    }
}