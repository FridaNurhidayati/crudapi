﻿using AutoMapper;
using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class ProductDetailViewModel : ProductModel
    {

        public ProductDetailViewModel()
        {

        }
        public ProductDetailViewModel(Product product)
        {
            ProductID = product.ProductID;
            ProductName = product.ProductName;
            SupplierID = product.SupplierID;
            CategoryID = product.CategoryID;
            QuantityPerUnit = product.QuantityPerUnit;
            UnitPrice = product.UnitPrice;
            UnitsInStock = product.UnitsInStock;
            UnitsOnOrder = product.UnitsOnOrder;
            ReorderLevel = product.ReorderLevel;
            Discontinued = product.Discontinued;
            ProductType = product.ProductType;
            if (ProductType != null)
            {
                if (ProductType.Contains("FoodsAndBeverageItems"))
                {
                    ProductDetailFoodsAndBeverageItems viewFood = new ProductDetailFoodsAndBeverageItems(product);
                    ProductDetail = viewFood.Prod();
                }else if (ProductType.Contains("GarmentItems"))
                {
                    ProductDetailGarmentItems viewGarment = new ProductDetailGarmentItems(product);
                    ProductDetail = viewGarment.Prod();
                }else if (ProductType.Contains("MaterialItems"))
                {
                    ProductDetailMaterialItems viewMaterial = new ProductDetailMaterialItems(product);
                    ProductDetail = viewMaterial.Prod();
                }
                else if (ProductType.Contains("TransportationServices"))
                {
                    ProductDetailTransportaionServices viewTransportasi = new ProductDetailTransportaionServices();
                    ProductDetail = viewTransportasi.Prod();
                }
                else if (ProductType.Contains("TelecommunicationServices"))
                {
                    ProductDetailTelecommunicationServices viewTelecommunication = new ProductDetailTelecommunicationServices();
                    ProductDetail = viewTelecommunication.Prod();
                }
            }
            
        }

        public Product ConvertToProduct(string condition = "", int userDemand = 0, int duration = 0)
        {
            var detail = "";
            decimal unit_price = 0;
            var config = new MapperConfiguration(kon => { });
            var map = new Mapper(config);
            if (this.ProductType.Contains("FoodsAndBeverageItems"))
            {
                ProductDetailFoodsAndBeverageItems foodsAndBeverageItems = map.Map<ProductDetailFoodsAndBeverageItems>(this.ProductDetail);
                detail = foodsAndBeverageItems.ConvertItemsToString();
                unit_price = foodsAndBeverageItems.CalculateProducTotUnitPrice();
            }else if(this.ProductType.Contains("GarmentItems"))
            {
                ProductDetailGarmentItems garmentItems = map.Map<ProductDetailGarmentItems>(this.ProductDetail);
                detail = garmentItems.ConvertItemsToString();
                unit_price = garmentItems.CalculateProducTotUnitPrice();
            }else if (this.ProductType.Contains("MaterialItems"))
            {
                ProductDetailMaterialItems materialItems = map.Map<ProductDetailMaterialItems>(this.ProductDetail);
                detail = materialItems.ConvertItemsToString();
                unit_price = materialItems.CalculateProducTotUnitPrice();
            }else if (this.ProductType.Contains("TransportationServices"))
            {
                ProductDetailTransportaionServices transportaionServices = map.Map<ProductDetailTransportaionServices>(this.ProductDetail);
                detail = transportaionServices.ConvertServicesToString();
                unit_price = transportaionServices.CalculateProducTotUnitPrice(condition, userDemand, duration);
            }else if (this.ProductType.Contains("TelecommunicationServices"))
            {
                ProductDetailTelecommunicationServices telecommunicationServices = map.Map<ProductDetailTelecommunicationServices>(this.ProductDetail);
                detail = telecommunicationServices.ConvertServicesToString();
                unit_price = telecommunicationServices.CalculateProducTotUnitPrice(condition, userDemand, duration);
            }
            return new Product()
            {
                ProductID = this.ProductID,
                ProductName = this.ProductName,
                SupplierID = this.SupplierID,
                CategoryID = this.CategoryID,
                QuantityPerUnit = this.QuantityPerUnit,
                UnitPrice = unit_price,
                UnitsInStock = this.UnitsInStock,
                UnitsOnOrder = this.UnitsOnOrder,
                ReorderLevel = this.ReorderLevel,
                Discontinued = this.Discontinued,
                ProductType = this.ProductType,
                ProductDetail = detail
            };
        }
       
    }
}