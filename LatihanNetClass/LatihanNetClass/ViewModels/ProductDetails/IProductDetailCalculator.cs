﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LatihanNetClass.ViewModels.ProductDetails
{
    interface IProductDetailCalculator
    {
        void setAdditionalParameter(ProductDetailCalculatorParameter parameter);
        decimal calculateProductCost();
    }
}
