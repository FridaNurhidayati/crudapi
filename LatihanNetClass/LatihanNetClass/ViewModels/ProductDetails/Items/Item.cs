﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.ProductDetails.Items
{
    public abstract class Item : ProductDetail
    {
        public Item(char Delimiter) : base(Delimiter)
        {

        }
        public string ProductionCode { get; set; }
        public string ProductionDate { get; set; }
        public string UnitOfMeasurement { get; set; }
        public override void setAdditionalParameter(ProductDetailCalculatorParameter parameter)
        {

        }
        public override decimal calculateProductCost()
        {
            return decimal.Parse(this.CostRate);
        }
    }
}