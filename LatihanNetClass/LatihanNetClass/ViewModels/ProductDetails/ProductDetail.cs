﻿using LatihanNetClass.ViewModels.ProductDetails.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.ProductDetails
{
    public abstract class ProductDetail : IProductDetail, IProductDetailCalculator
    {
        public readonly decimal RateCalculation = (decimal)(110.00 / 100.00);
        public int ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string CostRate { get; set; }
        public char Delimiter { get; set; }
        public ProductDetail(char Delimiter)
        {
            this.Delimiter = Delimiter;
        }

        public decimal getDecCostRate()
        {
            return this.RateCalculation;
        }
        public string appendWithDelimiter(params object[] listParam)
        {
            string result = "";
            char delimiter = (char) 0;
            foreach(object param in listParam)
            {
                result += delimiter + param.ToString();
                delimiter = this.Delimiter;
            }
            return result;
        }
        public abstract string ConvertToString();
        public abstract Dictionary<string, object> ConvertToDictionary();
        public abstract void setAdditionalParameter(ProductDetailCalculatorParameter parameter);
        public abstract decimal calculateProductCost();
    }
}