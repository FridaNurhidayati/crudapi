﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels.ProductDetails.Items.Details;
using LatihanNetClass.ViewModels.ProductDetails.Services.Details;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.ProductDetails
{
    public class ProductDetailCreate : ProductModel
    {
        private char Delimiter;
        public ProductDetailCreate()
        {

        }

        public Product ConvertToProduct()
        {
            Product product = new Product();
            var detail = "";
            if (this.ProductType.Contains("FoodsAndBeverageItems"))
            {
                FoodAndBeverage food = new FoodAndBeverage(this.Delimiter, product);
                detail = food.ConvertToString();
            }
            else if (this.ProductType.Contains("GarmentItems"))
            {
                Garment garment = new Garment(this.Delimiter, product);
                detail = garment.ConvertToString();
            }
            else if (this.ProductType.Contains("MaterialItems"))
            {
                Material material = new Material(this.Delimiter, product);
                detail = material.ConvertToString();
            }
            else if (this.ProductType.Contains("TransportationServices"))
            {
                Transportation transport = new Transportation(this.Delimiter, product);
                detail = transport.ConvertToString();
            }
            else if (this.ProductType.Contains("TelecommunicationServices"))
            {
                Telecommunication telecom = new Telecommunication(this.Delimiter, product);
                detail = telecom.ConvertToString();
            }
            return new Product()
            {
                ProductID = this.ProductID,
                ProductName = this.ProductName,
                SupplierID = this.SupplierID,
                CategoryID = this.CategoryID,
                QuantityPerUnit = this.QuantityPerUnit,
                UnitPrice = this.UnitPrice,
                UnitsInStock = this.UnitsInStock,
                UnitsOnOrder = this.UnitsOnOrder,
                ReorderLevel = this.ReorderLevel,
                Discontinued = this.Discontinued,
                ProductType = this.ProductType,
                ProductDetail = detail
            };
        }
    }


    }
}