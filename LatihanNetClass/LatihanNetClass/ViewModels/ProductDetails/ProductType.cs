﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels.ProductDetails.Items.Details;
using LatihanNetClass.ViewModels.ProductDetails.Services.Details;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.ProductDetails
{
    public class ProductType
    {
        private char Delimiter;
        public ProductType(char Delimiter)
        {
            this.Delimiter = Delimiter;
        }
        public ProductType()
        {
            ProductDetail productDetail= null;
            Product product = new Product();
            if (product.Equals("FoodsAndBeverageItems"))
            {
                productDetail = new FoodAndBeverage(this.Delimiter, product);
            }
            else if (product.ProductType.Contains("MaterialItems"))
            {
                productDetail = new Material(this.Delimiter, product);
            }
            else if (product.ProductType.Contains("GarmentItems"))
            {
                productDetail = new Garment(this.Delimiter, product);
            }
            else if (product.ProductType.Contains("TransportationServices"))
            {
                productDetail = new Transportation(this.Delimiter, product);
            }
            else if (product.ProductType.Contains("TelecommunicationServices"))
            {
                productDetail = new Telecommunication(this.Delimiter, product);
            }
        }
    }
}