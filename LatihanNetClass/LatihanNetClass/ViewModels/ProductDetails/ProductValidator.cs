﻿using LatihanNetClass.EntityFrameworks;
using LatihanNetClass.ViewModels.ProductDetails.Items;
using LatihanNetClass.ViewModels.ProductDetails.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.ProductDetails
{
    public class ProductValidator
    {
        public bool IsValidProductDetail(ProductDetail productDetail, string productType)
        {
            ProductType tipe = new ProductType();
            if (productDetail.GetType() == typeof(Item) && productDetail.Delimiter == '|' && productType == tipe.ToString())
            { 
                return true;
                
            }else if(productDetail.GetType() == typeof(Service) && productDetail.Delimiter == ';' && productType == tipe.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsValidProductDetail(Dictionary<string, object> productDetail, string productType)
        {
            ProductType tipe = new ProductType();
            if (productDetail.GetType() == typeof(Item) && productDetail.ContainsValue('|') && productType == tipe.ToString())
            {
                return true;
            }
            else if (productDetail.GetType() == typeof(Service) && productDetail.ContainsValue(';') && productType == tipe.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsValidProductDetail(string productDetail, string productType)
        {
            Product product = new Product();
            ProductType tipe = new ProductType();
            if (productDetail.GetType() == typeof(Item) && productDetail.Contains('|') && productType == tipe.ToString())
            {
                return true;
            }
            else if (productDetail.GetType() == typeof(Service) && productDetail.Contains(';') && productType == tipe.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}