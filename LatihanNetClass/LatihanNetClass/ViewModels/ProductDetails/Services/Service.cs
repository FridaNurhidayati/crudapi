﻿using LatihanNetClass.ViewModels.ProductDetails.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels.ProductDetails.Services
{
    public abstract class Service : ProductDetail
    {
        public ProductDetailCalculatorParameter parameter { get; set; }
        public Service(char Delimiter) : base(Delimiter)
        {

        }
        public string CostCalculationMethod { get; set; }
        public override void setAdditionalParameter(ProductDetailCalculatorParameter parameter)
        {
            this.parameter = parameter;
        }
    }
}