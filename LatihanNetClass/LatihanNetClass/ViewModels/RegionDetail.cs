﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class RegionDetail
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public string RegionLangitude { get; set; }
        public string RegionLatitude { get; set; }
        public string Country { get; set; }
        public RegionDetail()
        {

        }

        public RegionDetail(Region region)
        {
            char[] delimiter = { '|' };
            this.RegionID = region.RegionID;

            if (!string.IsNullOrEmpty(region.RegionDescription))
            {
                String[] regionDetailData = region.RegionDescription.Split(delimiter);

                if (regionDetailData.Length == 4)
                {
                    this.RegionName = regionDetailData[0];
                    this.RegionLangitude = regionDetailData[1];
                    this.RegionLatitude = regionDetailData[2];
                    this.Country = regionDetailData[3];
                }
            }
        }
        public Region convertToRegion()
        {
            char[] delimiter = { '|' };
            return new Region()
            {
                RegionID = this.RegionID,
                RegionDescription =
                    this.RegionName + delimiter[0] +
                    this.RegionLangitude + delimiter[0] +
                    this.RegionLatitude + delimiter[0] +
                    this.Country,
            };
        }

        public Territory toTerritority(DB_Context db)
        {       
            if (this.Country.Contains("INA")){
                Territory newTerritory = new Territory()
                {
                    TerritoryID = "INA-01",
                    TerritoryDescription = "Bandung salah satu kotanya",
                    RegionID = this.RegionID
                };
                db.Territories.Add(newTerritory);
                db.SaveChanges();
            }
            return new Territory();
        }
    }
}