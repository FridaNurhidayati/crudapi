﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    interface ServicesViewModel
    {
       
        Dictionary<string, object> Prod();
        String ConvertServicesToString();
        decimal CalculateProducTotUnitPrice(string condition = "", int userDemand = 0, int duration = 0);
        char delimiter();
    }

    
}