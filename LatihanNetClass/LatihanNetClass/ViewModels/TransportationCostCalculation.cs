﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class TransportationCostCalculation
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string CostCalculationMethod { get; set; }
        public int CostCalculation { get; set; }
        private int CostRate { get; set; }
        private int RouteMileage { get; set; }
        public TransportationCostCalculation()
        {

        }

        public TransportationCostCalculation(Product product, string Condition, int UserDemand)
        {
            char[] delimiter = { '|' };
            this.ProductID = product.ProductID.ToString();
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                this.ProductName = product.ProductName;
                String[] pisah = product.ProductDetail.Split(delimiter);
                this.RouteMileage = Int32.Parse(pisah[3]);
                this.CostCalculationMethod = pisah[4];
                this.CostRate = Int32.Parse(pisah[5]);
                this.CostCalculation = Kalkulasi(Condition, UserDemand);
            }
        }
        public int Kalkulasi(string Condition, int UserDemand)
        {
            var result = 0;
            if (this.CostCalculationMethod.Equals("FixPerRoute"))
            {
                result = 1 * this.CostRate;
            }
            else if (this.CostCalculationMethod.Equals("PerMiles"))
            {
                result = (this.RouteMileage * this.CostRate) / 2;
            }else if (this.CostCalculationMethod.Equals("PerMilesWithCondition"))
            {
                var valcon = 0;
                if (Condition == "GoodWeather")
                {
                    valcon = 5;
                }else if (Condition == "BadWeather")
                {
                    valcon = 15;
                }
                result = ((this.RouteMileage * this.CostRate) / 2) * (((valcon + (UserDemand / 50)) + 95) /100);
            }

            return result;
        }
    }
}