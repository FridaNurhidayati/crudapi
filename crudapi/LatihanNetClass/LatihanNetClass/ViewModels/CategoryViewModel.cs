﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class CategoryViewModel
    {
        public int CategoryID { get; set; }

        public string CategoryName { get; set; }

        public string Descripttion { get; set; }

        public byte[] Picture { get; set; }

        public CategoryViewModel()
        {

        }

        public CategoryViewModel (Category entity)
        {
            CategoryID = entity.CategoryID;
            CategoryName = entity.CategoryName;
            Descripttion = entity.Description;
            Picture = entity.Picture;
        }
    }

   
}