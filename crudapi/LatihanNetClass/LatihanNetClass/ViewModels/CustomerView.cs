﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class CustomerView
    {
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public List<OrderViewModel> product_list { get; set; }
       
    }
}