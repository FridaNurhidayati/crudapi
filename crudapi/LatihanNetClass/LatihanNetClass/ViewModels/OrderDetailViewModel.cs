﻿using LatihanNetClass.EntityFrameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LatihanNetClass.ViewModels
{
    public class OrderDetailViewModel
    {
        private Order item;

        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string SupplierName { get; set; }
        public decimal UnitPrice { get; set; }
        public short Quantity { get; set; }

        public decimal Total { get; set; }
        public OrderDetailViewModel(Order_Detail entity)
        {
            ProductID = entity.ProductID;
            ProductName = entity.Product.ProductName;
            UnitPrice = entity.UnitPrice;
            Quantity = entity.Quantity;
            CategoryName = entity.Product.Category.CategoryName;
            SupplierName = entity.Product.Supplier.CompanyName;
            Total = (Convert.ToDecimal(entity.UnitPrice) * Convert.ToDecimal(entity.Quantity)) - (Convert.ToDecimal(entity.UnitPrice) * Convert.ToDecimal(entity.Quantity) * Convert.ToDecimal(entity.Discount));
        }

    }
}